import Vue from 'vue'
import axios from 'axios'

import App from './App'
import router from './router'
import VueEvents from 'vue-events'
import {ClientTable} from 'vue-tables-2'
import wysiwyg from 'vue-wysiwyg'
import './assets/css/main.css'
import SortControl from './components/table/SortControl'

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false
Vue.use(VueEvents)
Vue.use(ClientTable, {}, false, 'bootstrap4', {
  sortControl: SortControl
})
Vue.use(wysiwyg, {})

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  template: '<App/>'
}).$mount('#app')
