import { Exam } from '../models/exam'
import { Student } from '../models/student'
import { Strand } from '../models/strand'
import { Result } from '../models/result'
import { Question } from '../models/question'
import { pool } from '../configurations/db'
import { Choice } from '../models/choice'
const { Op } = require('sequelize')
const sequelize = require('sequelize')
const _ = require('lodash')
const math = require('mathjs')

/**
 * Get all femaile and male who pass and failed
 */
export const getPassAndFailByScore = async (filterYear = '*') => {
  try {
    let wherePassParams = {
      grade: '1'
    }
    let whereFailedParams = {
      grade: '0'
    }
    if (filterYear !== '*') {
      wherePassParams[Op.and] = sequelize.where(sequelize.fn('YEAR', sequelize.col('created_at')), filterYear)
      whereFailedParams[Op.and] = sequelize.where(sequelize.fn('YEAR', sequelize.col('created_at')), filterYear)
    }
    const passStudents = await Student.findAndCountAll({
      attributes: [
        'score',
        [sequelize.fn('COUNT', sequelize.col('*')), 'passed']
      ],
      where: wherePassParams,
      group: ['score'],
      order: [['score', 'ASC']]
    })
    const failedStudents = await Student.findAndCountAll({
      attributes: ['score', [sequelize.fn('COUNT', sequelize.col('*')), 'failed']],
      where: whereFailedParams,
      group: ['score'],
      order: [['score', 'ASC']]
    })

    let pass = []
    passStudents.count.map(passStudent => {
      pass.push({
        x: passStudent.score,
        y: passStudent.count
      })
    })
    let failed = []
    failedStudents.count.map(failedStudent => {
      failed.push({
        x: failedStudent.score,
        y: failedStudent.count
      })
    })
    return {
      pass,
      failed
    }
  } catch (error) {
    throw new Error(error)
  }
}

/**
 * Get all femaile and male who pass and failed
 */
export const getPassAndFailByGender = async (filterYear = '*') => {
  try {
    const exam = await Exam.findByPk(1)
    let wherePassParams = {
      score: {
        [Op.gte]: exam.dataValues.passing_grade
      }
    }
    let whereFailedParams = {
      score: {
        [Op.lt]: exam.dataValues.passing_grade
      }
    }
    if (filterYear !== '*') {
      wherePassParams[Op.and] = sequelize.where(sequelize.fn('YEAR', sequelize.col('created_at')), filterYear)
      whereFailedParams[Op.and] = sequelize.where(sequelize.fn('YEAR', sequelize.col('created_at')), filterYear)
    }
    const passStudents = await Student.findAndCountAll({
      attributes: [
        'gender',
        [sequelize.fn('COUNT', sequelize.col('*')), 'passed']
      ],
      where: wherePassParams,
      group: ['gender']
    })
    const failedStudents = await Student.findAndCountAll({
      attributes: ['gender', [sequelize.fn('COUNT', sequelize.col('*')), 'failed']],
      where: whereFailedParams,
      group: ['gender']
    })

    let pass = [0, 0]
    passStudents.count.map(passStudent => {
      if (passStudent.gender === 'M') {
        pass[0] = passStudent.count
      }
      if (passStudent.gender === 'F') {
        pass[1] = passStudent.count
      }
    })
    let failed = [0, 0]
    failedStudents.count.map(failedStudent => {
      if (failedStudent.gender === 'M') {
        failed[0] = failedStudent.count
      }
      if (failedStudent.gender === 'F') {
        failed[1] = failedStudent.count
      }
    })
    return {
      pass,
      failed
    }
  } catch (error) {
    throw new Error(error)
  }
}

/**
 * Get all result who pass and failed by school type
 */
export const getPassAndFailBySchoolType = async (filterYear = '*') => {
  try {
    const exam = await Exam.findByPk(1)
    let wherePassParams = {
      score: {
        [Op.gte]: exam.dataValues.passing_grade
      }
    }
    let whereFailedParams = {
      score: {
        [Op.lt]: exam.dataValues.passing_grade
      }
    }
    if (filterYear !== '*') {
      wherePassParams[Op.and] = sequelize.where(sequelize.fn('YEAR', sequelize.col('created_at')), filterYear)
      whereFailedParams[Op.and] = sequelize.where(sequelize.fn('YEAR', sequelize.col('created_at')), filterYear)
    }
    const passStudents = await Student.findAndCountAll({
      attributes: [
        'school_type',
        [sequelize.fn('COUNT', sequelize.col('*')), 'passed']
      ],
      where: wherePassParams,
      group: ['school_type']
    })
    const failedStudents = await Student.findAndCountAll({
      attributes: ['school_type', [sequelize.fn('COUNT', sequelize.col('*')), 'failed']],
      where: whereFailedParams,
      group: ['school_type']
    })
    let pass = [0, 0]
    passStudents.count.map(passStudent => {
      if (passStudent.school_type === 'PUBLIC') {
        pass[0] = passStudent.count
      }
      if (passStudent.school_type === 'PRIVATE') {
        pass[1] = passStudent.count
      }
    })
    let failed = [0, 0]
    failedStudents.count.map(failedStudent => {
      if (failedStudent.school_type === 'PUBLIC') {
        failed[0] = failedStudent.count
      }
      if (failedStudent.school_type === 'PRIVATE') {
        failed[1] = failedStudent.count
      }
    })
    return {
      pass,
      failed
    }
  } catch (error) {
    throw new Error(error)
  }
}

/**
 * Get all result who pass or fail by strand
 */
export const getPassAndFailByStrand = async (filterYear = '*') => {
  try {
    const exam = await Exam.findByPk(1)
    let wherePassParams = {
      score: {
        [Op.gte]: exam.dataValues.passing_grade
      }
    }
    let whereFailedParams = {
      score: {
        [Op.lt]: exam.dataValues.passing_grade
      }
    }
    if (filterYear !== '*') {
      wherePassParams[Op.and] = sequelize.where(sequelize.fn('YEAR', sequelize.col('created_at')), filterYear)
      whereFailedParams[Op.and] = sequelize.where(sequelize.fn('YEAR', sequelize.col('created_at')), filterYear)
    }
    const passStudents = await Student.findAndCountAll({
      attributes: [
        'strand_id',
        [sequelize.fn('COUNT', sequelize.col('*')), 'passed']
      ],
      where: wherePassParams,
      group: ['strand_id']
    })
    const failedStudents = await Student.findAndCountAll({
      attributes: ['strand_id', [sequelize.fn('COUNT', sequelize.col('*')), 'failed']],
      where: whereFailedParams,
      group: ['strand_id']
    })

    const rawStrands = await Strand.findAll()
    let pass = []
    let failed = []
    const strands = rawStrands.map(strand => {
      pass.push(0)
      failed.push(0)
      return strand.dataValues.code
    })

    for (let index = 0; index < failedStudents.count.length; index++) {
      const strand = await Strand.findByPk(failedStudents.count[index].strand_id)
      const strandIndex = strands.indexOf(strand.dataValues.code)
      failed[strandIndex] = failedStudents.count[index].count
    }
    for (let index = 0; index < passStudents.count.length; index++) {
      const strand = await Strand.findByPk(passStudents.count[index].strand_id)
      const strandIndex = strands.indexOf(strand.dataValues.code)
      pass[strandIndex] = passStudents.count[index].count
    }

    return {
      pass,
      failed,
      strands
    }
  } catch (error) {
    throw new Error(error)
  }
}

export const getPassAndFailByQuestion = async (filterYear = '*', subjectId = '*') => {
  try {
    let wherePassParams = {
      is_correct: 1
    }
    let whereFailedParams = {
      is_correct: 0
    }
    let questionParams = {}

    if (filterYear !== '*') {
      wherePassParams[Op.and] = sequelize.where(sequelize.fn('YEAR', sequelize.col('created_at')), filterYear)
      whereFailedParams[Op.and] = sequelize.where(sequelize.fn('YEAR', sequelize.col('created_at')), filterYear)
    }
    if (subjectId !== '*') {
      wherePassParams.subject_id = subjectId
      whereFailedParams.subject_id = subjectId
      questionParams.subject_id = subjectId
    }

    const correctPerQuestion = await Result.findAndCountAll({
      attributes: [
        'question_id',
        [sequelize.fn('COUNT', sequelize.col('*')), 'count']
      ],
      where: wherePassParams,
      group: ['question_id']
    })
    const wrongPerQuestion = await Result.findAndCountAll({
      attributes: [
        'question_id',
        [sequelize.fn('COUNT', sequelize.col('*')), 'count']
      ],
      where: whereFailedParams,
      group: ['question_id']
    })

    const listOfQuestions = await Question.findAll({
      where: questionParams
    })
    let passData = listOfQuestions.map(question => {
      return {
        x: question.dataValues.id,
        y: 0
      }
    })
    let failedData = listOfQuestions.map(question => {
      return {
        x: question.dataValues.id,
        y: 0
      }
    })

    await correctPerQuestion.count.map(correctAnswer => {
      const questionIndex = _.findIndex(passData, ['x', correctAnswer.question_id])
      passData[questionIndex].y = correctAnswer.count
    })

    await wrongPerQuestion.count.map(wrongAnswer => {
      const questionIndex = _.findIndex(passData, ['x', wrongAnswer.question_id])
      failedData[questionIndex].y = wrongAnswer.count
    })

    return {
      pass: passData,
      failed: failedData
    }
  } catch (error) {
    throw new Error(error)
  }
}

/**
 * Get all the number of selected answer per question
 */
export const getAllAnswerByQuestion = async (filterYear = '*', subjectId) => {
  try {
    let whereParams = {
      subject_id: subjectId
    }
    let queryStatement = 'SELECT question_id, choice_id, COUNT(*) AS count FROM results AS results GROUP BY choice_id, question_id'
    let options = {}
    if (filterYear !== '*' && filterYear !== '') {
      queryStatement = 'SELECT question_id, choice_id, COUNT(*) AS count FROM results AS results WHERE YEAR(created_at) = :year GROUP BY choice_id, question_id'
      options = {
        replacements: { year: filterYear }
      }
    }

    const results = await pool.query('SELECT question_id, COUNT(*) AS count FROM choices AS choices GROUP BY question_id ORDER BY COUNT(*) DESC')
    const questions = await Question.findAll({
      where: whereParams,
      include: [Choice]
    })

    if (results[1].length === 0) {
      return {
        datasets: [],
        labels: await questions.map(question => `QID: ${question.id}`)
      }
    }

    let rawReport = []
    const choicesList = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K']
    const colors = [
      {
        light: 'lightgreen',
        default: 'green'
      },
      {
        light: 'lightblue',
        default: 'blue'
      },
      {
        light: 'pink',
        default: 'red'
      },
      {
        light: 'violet',
        default: 'violet'
      },
      {
        light: 'gray',
        default: 'black'
      },
      {
        light: 'yellow',
        default: 'orange'
      }
    ]
    for (let index = 0; index < results[1][0].count; index++) {
      rawReport.push({
        label: choicesList[index],
        backgroundColor: colors[index].light,
        borderColor: colors[index].default,
        borderWidth: 1,
        choice_ids: [],
        data: []
      })
    }
    questions.map(question => {
      question.dataValues.choices.map((choice, index) => {
        rawReport[index].choice_ids.push(choice.dataValues.id)
        rawReport[index].data.push({
          x: `QID: ${question.dataValues.id}`,
          y: 0
        })
      })
    })

    const queryResults = await pool.query(queryStatement, options)
    if (queryResults[1].length === 0) {
      return {
        datasets: [],
        labels: await questions.map(question => `QID: ${question.id}`)
      }
    }

    queryResults[1].map(result => {
      rawReport.map((report, index) => {
        if (_.includes(report.choice_ids, result.choice_id)) {
          const questionIndex = _.findIndex(report.data, ['x', `QID: ${result.question_id}`])
          rawReport[index].data[questionIndex].y = result.count
        }
      })
    })
    return {
      datasets: rawReport,
      labels: await questions.map(question => `QID: ${question.id}`)
    }
  } catch (error) {
    throw new Error(error)
  }
}

/**
 * Get all student ZScore
 */
export const getStudentZScore = async () => {
  try {
    const students = await Student.findAll({
      where: {
        grade: {
          [Op.not]: 2
        }
      }
    })

    let grades = []

    students.map(student => {
      grades.push(student.score)
    })

    const mean = math.mean(grades)

    const std = math.std(grades, 'uncorrected')

    const reports = students.map(student => {
      return {
        name: `${student.first_name} ${student.last_name}`,
        score: student.score,
        zscore: (student.score - mean) / std
      }
    })

    return reports
  } catch (error) {
    throw new Error(error)
  }
}

export const getPassAndFail = async (filterYear = '*') => {
  try {
    let wherePassParams = {
      grade: 1
    }
    let whereFailedParams = {
      grade: 0
    }
    if (filterYear !== '*') {
      wherePassParams[Op.and] = sequelize.where(sequelize.fn('YEAR', sequelize.col('created_at')), filterYear)
      whereFailedParams[Op.and] = sequelize.where(sequelize.fn('YEAR', sequelize.col('created_at')), filterYear)
    }
    const passStudents = await Student.findAndCountAll({
      attributes: [
        [sequelize.fn('COUNT', sequelize.col('*')), 'passed']
      ],
      where: wherePassParams
    })
    const failedStudents = await Student.findAndCountAll({
      attributes: [[sequelize.fn('COUNT', sequelize.col('*')), 'failed']],
      where: whereFailedParams
    })

    return {
      pass: passStudents.count,
      failed: failedStudents.count
    }
  } catch (error) {
    throw new Error(error)
  }
}
