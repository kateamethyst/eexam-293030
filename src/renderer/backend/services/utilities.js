import createReport from 'docx-templates'
import fs from 'fs'
import moment from 'moment'
import { Student } from '../models/student'
const sequelize = require('sequelize')
// const path = require('path')

export const generateSlug = () => {
  return (Math.random().toString(36).substring(2, 6) + Math.random().toString(36).substring(2, 6)).toUpperCase()
}

export const formatCompleteTimestamp = (dateTime) => {
  return moment(dateTime).format('D MMM YYYY  h:mm:ss A')
}

export const getAllYear = async () => {
  try {
    const listOfYears = await Student.findAll({
      attributes: [
        [sequelize.fn('DISTINCT', sequelize.fn('YEAR', sequelize.col('created_at'))), 'year']
      ]
    })

    if (listOfYears.length === 0) {
      return ['2020']
    }
    const years = listOfYears.map(list => {
      return list.dataValues.year
    })
    return years
  } catch (error) {
    throw new Error(error)
  }
}

export const generateReport = async (examinee) => {
  // return path.join('src/assets', '.', 'README.md')
  const template = fs.readFileSync(`/Users/maangelica/Downloads/ExamResultTemplate.docx`)
  const buffer = await createReport({
    template,
    cmdDelimiter: ['{', '}'],
    data: {
      status: examinee.grade === '1' ? 'passed' : 'failed',
      percentage: examinee.score,
      name: `${examinee.first_name} ${examinee.last_name}`,
      date: moment(new Date()).format('MMMM DD, YYYY')
    }
  })
  fs.writeFileSync(`/Users/maangelica/Documents/ExamResult/${examinee.first_name}${examinee.last_name}-ExamResult.pdf`, buffer)

  return `/Users/maangelica/Documents/ExamResult/${examinee.first_name}${examinee.last_name}-ExamResult.pdf`
}
