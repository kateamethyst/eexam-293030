export const Sequelize = require('sequelize')

let DB_USERNAME = window.localStorage.getItem('db_username')
let DB_SCHEMA = window.localStorage.getItem('db_schema')
let DB_PASS = window.localStorage.getItem('db_password')
let DB_HOST = window.localStorage.getItem('db_host')
let DB_PORT = window.localStorage.getItem('db_port')

export const pool = new Sequelize(DB_SCHEMA, DB_USERNAME, DB_PASS, {
  dialect: 'mysql',
  host: DB_HOST,
  port: DB_PORT,
  define: {
    underscored: false,
    freezeTableName: false,
    charset: 'utf8',
    dialectOptions: {
      collate: 'utf8_general_ci'
    },
    timestamps: true
  },
  pool: {
    max: 50,
    min: 0,
    acquire: 60000,
    idle: 10000
  },
  hooks: {
    beforeConnect: (config) => {
      config.username = window.localStorage.getItem('db_username')
      config.database = window.localStorage.getItem('db_schema')
      config.password = window.localStorage.getItem('db_password')
      config.host = window.localStorage.getItem('db_host')
      config.port = window.localStorage.getItem('db_port')
    }
  }
})
