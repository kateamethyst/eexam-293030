import { Strand } from '../models/strand'

export const getStrands = () => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Strand.findAll({
        where: {
          is_active: '1'
        }
      }))
    } catch (error) {
      reject(error)
    }
  })
}

export const getAllStrands = () => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Strand.findAll())
    } catch (error) {
      reject(error)
    }
  })
}

export const addStrand = (strand) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Strand.create(strand))
    } catch (error) {
      reject(error)
    }
  })
}

export const updateStrand = (strand, strandId) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Strand.update(strand, {
        where: {
          id: strandId
        }
      }))
    } catch (error) {
      reject(error)
    }
  })
}
