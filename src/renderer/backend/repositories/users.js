import { User } from '../models/user'

export const addUser = (user) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(User.create(user))
    } catch (error) {
      reject(error)
    }
  })
}

export const updateUser = (user, userId) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(User.update(user, {
        where: {
          id: userId
        }
      }))
    } catch (error) {
      reject(error)
    }
  })
}

export const getUser = () => {
  return new Promise((resolve, reject) => {
    try {
      resolve(User.findAll())
    } catch (error) {
      reject(error)
    }
  })
}

export const findUser = (whereParams) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(User.findAll({
        where: whereParams
      }))
    } catch (error) {
      reject(error)
    }
  })
}
