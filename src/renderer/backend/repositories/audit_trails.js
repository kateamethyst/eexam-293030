import { AuditTrails } from '../models/audit_trails'
import { User } from '../models/user'

export const addTrail = (performedAction) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(AuditTrails.create(performedAction))
    } catch (error) {
      reject(error)
    }
  })
}

export const getAuditTrails = () => {
  return new Promise((resolve, reject) => {
    try {
      resolve(AuditTrails.findAll({
        order: [['created_at', 'DESC']],
        include: [ User ]
      }))
    } catch (error) {
      reject(error)
    }
  })
}
