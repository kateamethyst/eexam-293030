import { Exam } from '../models/exam'

export const getExam = () => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Exam.findByPk(1))
    } catch (error) {
      reject(error)
    }
  })
}

export const addExam = (exam) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Exam.create(exam))
    } catch (error) {
      reject(error)
    }
  })
}

export const updateExam = (exam, examId) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Exam.update(exam, {
        where: {
          id: examId
        }
      }))
    } catch (error) {
      reject(error)
    }
  })
}
