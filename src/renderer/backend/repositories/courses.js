import { Course } from '../models/course'

export const getCourses = () => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Course.findAll({
        where: {
          is_active: '1'
        }
      }))
    } catch (error) {
      reject(error)
    }
  })
}

export const getAllCourses = () => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Course.findAll())
    } catch (error) {
      reject(error)
    }
  })
}

export const addCourses = (course) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Course.create(course))
    } catch (error) {
      reject(error)
    }
  })
}

export const updateCourse = (course, courseId) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Course.update(course, {
        where: {
          id: courseId
        }
      }))
    } catch (error) {
      reject(error)
    }
  })
}
