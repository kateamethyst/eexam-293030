import { Subject } from '../models/subject'
import { Question } from '../models/question'
import { Choice } from '../models/choice'

export const getSubject = () => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Subject.findAll())
    } catch (error) {
      reject(error)
    }
  })
}

export const getSubjectById = (subjectId) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Subject.findByPk(subjectId))
    } catch (error) {
      reject(error)
    }
  })
}

export const addSubject = (subject) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Subject.create(subject))
    } catch (error) {
      reject(error)
    }
  })
}

export const updateSubject = (subject, subjectId) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Subject.update(subject, {
        where: {
          id: subjectId
        }
      }))
    } catch (error) {
      reject(error)
    }
  })
}

export const getCompleteExam = () => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Subject.findAll({
        where: {
          is_active: '1'
        },
        include: [
          {
            model: Question,
            where: {
              is_active: '1',
              is_deleted: '0'
            },
            include: [Choice]
          }
        ]
      }))
    } catch (error) {
      reject(error)
    }
  })
}
