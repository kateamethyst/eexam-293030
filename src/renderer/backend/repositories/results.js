import { Result } from '../models/result'

export const getResults = (whereParams) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Result.findAll({
        where: whereParams
      }))
    } catch (error) {
      reject(error)
    }
  })
}

export const addResults = (results) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Result.bulkCreate(results))
    } catch (error) {
      reject(error)
    }
  })
}
