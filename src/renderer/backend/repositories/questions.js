import { Question } from '../models/question'

export const getAllQuestions = () => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Question.findAll({
        order: [['createdAt', 'DESC']]
      }))
    } catch (error) {
      reject(error)
    }
  })
}

export const getQuestion = (whereParams) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Question.findAll({
        where: whereParams,
        order: [['createdAt', 'DESC']]
      }))
    } catch (error) {
      reject(error)
    }
  })
}

export const getTotalItems = () => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Question.findAndCountAll({
        where: {
          is_active: '1',
          is_deleted: '0'
        }
      }))
    } catch (error) {
      reject(error)
    }
  })
}

export const addQuestion = (question) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Question.create(question))
    } catch (error) {
      reject(error)
    }
  })
}

export const updateQuestion = (question, questionId) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Question.update(question, {
        where: {
          id: questionId
        }
      }))
    } catch (error) {
      reject(error)
    }
  })
}
