import { Choice } from '../models/choice'

export const getChoices = (whereParams) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Choice.findAll({
        where: whereParams
      }))
    } catch (error) {
      reject(error)
    }
  })
}

export const addChoices = (choices) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Choice.bulkCreate(choices))
    } catch (error) {
      reject(error)
    }
  })
}

export const deleteChoices = (questionId) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Choice.destroy({
        where: {
          question_id: questionId
        }
      }))
    } catch (error) {
      reject(error)
    }
  })
}
