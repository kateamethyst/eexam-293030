import { Student } from '../models/student'
import { Strand } from '../models/strand'
import { Course } from '../models/course'

export const getStudents = () => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Student.findAll({
        include: [Strand, Course]
      }))
    } catch (error) {
      reject(error)
    }
  })
}

export const getStudent = (whereParams) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Student.findAll({
        where: whereParams
      }))
    } catch (error) {
      reject(error)
    }
  })
}

export const addStudent = (student) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Student.create(student))
    } catch (error) {
      reject(error)
    }
  })
}

export const updateStudent = (student, studentId) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Student.update(student, {
        where: {
          id: studentId
        }
      }))
    } catch (error) {
      reject(error)
    }
  })
}
