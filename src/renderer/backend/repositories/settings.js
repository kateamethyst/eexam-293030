import { Setting } from '../models/setting'

export const getSettings = () => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Setting.findByPk(1))
    } catch (error) {
      reject(error)
    }
  })
}

export const updateReportPath = (settings) => {
  return new Promise((resolve, reject) => {
    try {
      resolve(Setting.update(settings, {
        where: {
          id: 1
        }
      }))
    } catch (error) {
      reject(error)
    }
  })
}
