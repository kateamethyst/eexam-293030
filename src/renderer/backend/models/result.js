import { Sequelize, pool } from '../configurations/db'

export const Result = pool.define('results', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  question_id: {
    type: Sequelize.INTEGER
  },
  choice_id: {
    type: Sequelize.INTEGER
  },
  subject_id: {
    type: Sequelize.INTEGER
  },
  student_id: {
    type: Sequelize.INTEGER
  },
  correct_answer: {
    type: Sequelize.INTEGER
  },
  is_correct: {
    type: Sequelize.STRING
  },
  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE
  }
})
