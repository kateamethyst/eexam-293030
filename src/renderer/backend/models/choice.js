import { Sequelize, pool } from '../configurations/db'
// import { Question } from './question'
export const Choice = pool.define('choices', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  slug: {
    type: Sequelize.STRING
  },
  text: {
    type: Sequelize.STRING
  },
  question_id: {
    type: Sequelize.INTEGER
  },
  is_answer: {
    type: Sequelize.STRING
  },
  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE
  }
})

// Choice.belongsTo(Question, {foreignKey: 'question_id'})
