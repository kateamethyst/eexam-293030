import { Sequelize, pool } from '../configurations/db'
import { User } from './user'
export const AuditTrails = pool.define('audit_trails', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  slug: {
    type: Sequelize.STRING
  },
  action: {
    type: Sequelize.STRING
  },
  module: {
    type: Sequelize.STRING
  },
  data: {
    type: Sequelize.TEXT,
    allowNull: true
  },
  user_id: {
    type: Sequelize.INTEGER
  },
  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE
  }
})

AuditTrails.belongsTo(User, {foreignKey: 'user_id'})
