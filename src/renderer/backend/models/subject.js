import { Sequelize, pool } from '../configurations/db'
import {Question} from './question'
export const Subject = pool.define('subjects', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  slug: {
    type: Sequelize.STRING
  },
  name: {
    type: Sequelize.STRING
  },
  description: {
    type: Sequelize.STRING
  },
  is_active: {
    type: Sequelize.STRING
  },
  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE
  }
})

Subject.hasMany(Question, {foreignKey: 'subject_id'})
