import { Sequelize, pool } from '../configurations/db'
// import {Subject} from './subject'
import { Choice } from './choice'
export const Question = pool.define('questions', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  slug: {
    type: Sequelize.STRING
  },
  title: {
    type: Sequelize.STRING
  },
  description: {
    type: Sequelize.TEXT
  },
  exam_id: {
    type: Sequelize.INTEGER
  },
  subject_id: {
    type: Sequelize.INTEGER
  },
  is_active: {
    type: Sequelize.STRING
  },
  is_deleted: {
    type: Sequelize.STRING
  },
  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE
  }
})

// Question.belongsTo(Subject, {foreignKey: 'subject_id'})
Question.hasMany(Choice, {foreignKey: 'question_id'})
