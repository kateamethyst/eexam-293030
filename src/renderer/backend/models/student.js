import { Sequelize, pool } from '../configurations/db'
import { Strand } from './strand'
import { Course } from './course'

export const Student = pool.define('students', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  slug: {
    type: Sequelize.STRING
  },
  first_name: {
    type: Sequelize.STRING
  },
  last_name: {
    type: Sequelize.STRING
  },
  middle_initial: {
    type: Sequelize.STRING
  },
  gender: {
    type: Sequelize.ENUM(['M', 'F'])
  },
  birthdate: {
    type: Sequelize.DATE
  },
  date_taken: {
    type: Sequelize.DATE,
    allowNull: true
  },
  score: {
    type: Sequelize.INTEGER
  },
  grade: {
    type: Sequelize.STRING
  },
  last_attended_school: {
    type: Sequelize.STRING
  },
  school_type: {
    type: Sequelize.ENUM(['PUBLIC', 'PRIVATE'])
  },
  strand_id: {
    type: Sequelize.INTEGER
  },
  first_choice_course: {
    type: Sequelize.INTEGER
  },
  second_choice_course: {
    type: Sequelize.INTEGER
  },
  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE
  }
})

Student.belongsTo(Strand, {foreignKey: 'strand_id'})
Student.belongsTo(Course, {foreignKey: 'first_choice_course'})
