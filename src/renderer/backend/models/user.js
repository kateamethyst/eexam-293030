import { Sequelize, pool } from '../configurations/db'

export const User = pool.define('users', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  slug: {
    type: Sequelize.STRING
  },
  first_name: {
    type: Sequelize.STRING
  },
  last_name: {
    type: Sequelize.STRING
  },
  middle_name: {
    type: Sequelize.STRING
  },
  gender: {
    type: Sequelize.STRING
  },
  birthdate: {
    type: Sequelize.DATE
  },
  address: {
    type: Sequelize.STRING
  },
  mobile_number: {
    type: Sequelize.STRING
  },
  email: {
    type: Sequelize.STRING
  },
  username: {
    type: Sequelize.STRING,
    allowNull: true
  },
  password: {
    type: Sequelize.STRING,
    allowNull: true
  },
  is_active: {
    type: Sequelize.STRING
  },
  access: {
    type: Sequelize.ENUM(['STAFF', 'PROF', 'ADMIN'])
  },
  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE
  }
})
