import { Sequelize, pool } from '../configurations/db'

export const Exam = pool.define('exams', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  slug: {
    type: Sequelize.STRING
  },
  code: {
    type: Sequelize.STRING
  },
  name: {
    type: Sequelize.STRING
  },
  description: {
    type: Sequelize.STRING
  },
  instructions: {
    type: Sequelize.TEXT
  },
  passing_grade: {
    type: Sequelize.INTEGER
  },
  hours: {
    type: Sequelize.STRING
  },
  minutes: {
    type: Sequelize.STRING
  },
  user_id: {
    type: Sequelize.INTEGER
  },
  is_active: {
    type: Sequelize.STRING
  },
  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE
  }
})
