import Vue from 'vue'
import Router from 'vue-router'
import AdminLoginContainer from '../containers/AdminLoginContainer.vue'
import AuditTrailContainer from '../containers/dashboard/AuditTrailContainer.vue'
import UserContainer from '../containers/dashboard/UserContainer.vue'
import ExamContainer from '../containers/dashboard/ExamContainer.vue'
import DashboardContainer from '../containers/dashboard/DashboardContainer.vue'
import MyProfileContainer from '../containers/dashboard/MyProfileContainer.vue'
import QuestionContainer from '../containers/dashboard/QuestionContainer.vue'
import RegisterContainer from '../containers/RegisterContainer.vue'
import StudentContainer from '../containers/dashboard/StudentContainer.vue'
import SettingsContainer from '../containers/dashboard/SettingsContainer.vue'
import StudentExamContainer from '../containers/ExamContainer.vue'
import SubjectContainer from '../containers/SubjectContainer.vue'
import ByGenderContainer from '../containers/dashboard/reports/ByGenderContainer.vue'
import ByScoreContainer from '../containers/dashboard/reports/ByScoreContainer.vue'
import ByStrandContainer from '../containers/dashboard/reports/ByStrandContainer.vue'
import BySchoolContainer from '../containers/dashboard/reports/BySchoolContainer.vue'
import PerQuestionContainer from '../containers/dashboard/reports/PerQuestionContainer.vue'
import PerChoicesContainer from '../containers/dashboard/reports/PerChoicesContainer.vue'
import ExamResultContainer from '../containers/dashboard/reports/ExamResultContainer.vue'
import ZScoreContainer from '../containers/dashboard/reports/ZScoreContainer.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'landing-page',
      component: require('@/components/LandingPage').default
    },
    {
      path: '/login',
      name: 'login-page',
      component: AdminLoginContainer
    },
    {
      path: '/exam',
      name: 'exam',
      component: StudentExamContainer
    },
    {
      path: '/subject',
      name: 'subject',
      component: SubjectContainer
    },
    {
      path: '/register',
      name: 'register-student',
      component: RegisterContainer
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: DashboardContainer
    },
    {
      path: '/my-profile',
      name: 'my-profile',
      component: MyProfileContainer
    },
    {
      path: '/dashboard/audit-trails',
      name: 'audit-trails',
      component: AuditTrailContainer
    },
    {
      path: '/dashboard/users',
      name: 'users',
      component: UserContainer
    },
    {
      path: '/dashboard/settings',
      name: 'settings',
      component: SettingsContainer
    },
    {
      path: '/dashboard/exams',
      name: 'exams',
      component: ExamContainer
    },
    {
      path: '/dashboard/examinees',
      name: 'examinees',
      component: StudentContainer
    },
    {
      path: '/dashboard/subject/:id/questions',
      name: 'questions',
      component: QuestionContainer
    },
    {
      path: '/dashboard/data-analytics/gender',
      name: 'by-gender',
      component: ByGenderContainer
    },
    {
      path: '/dashboard/data-analytics/score',
      name: 'by-score',
      component: ByScoreContainer
    },
    {
      path: '/dashboard/data-analytics/strand',
      name: 'by-strand',
      component: ByStrandContainer
    },
    {
      path: '/dashboard/data-analytics/school',
      name: 'by-school',
      component: BySchoolContainer
    },
    {
      path: '/dashboard/item-analysis/question',
      name: 'per-question',
      component: PerQuestionContainer
    },
    {
      path: '/dashboard/item-analysis/choices',
      name: 'per-choices',
      component: PerChoicesContainer
    },
    {
      path: '/dashboard/z-score',
      name: 'z-score',
      component: ZScoreContainer
    },
    {
      path: '/dashboard/examinees/:id',
      name: 'exam-result',
      component: ExamResultContainer
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
